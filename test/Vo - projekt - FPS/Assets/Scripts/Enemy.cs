﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
	
	public int health=10;
	public int damage=25;
	public Player player;
	
	
	void OnTriggerEnter(Collider otherCollider){
		Debug.Log("hit");
		if(otherCollider.GetComponent<Bullet>()!=null){
			Debug.Log("collide with bullet");
			Bullet bullet= otherCollider.GetComponent<Bullet>();
			Debug.Log("Bullet damage"+ bullet.damage);
			health-=bullet.damage;
			
			bullet.gameObject.SetActive(false);
			
			if(health<=0){
				//player=GetComponent<Player>();
				//ammo=player.GetComponent<Player>().ammo
				player.kills=player.kills+1;
				//player.Kills;
				Destroy(gameObject);
				
			}
		}else if(otherCollider.GetComponent<Player>()!=null){
			Debug.Log("collide with player");
		}
	}
	
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}

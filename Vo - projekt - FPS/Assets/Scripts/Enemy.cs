﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static EnemyAI;

public class Enemy : MonoBehaviour
{
	
	public int health=10;
	public int damage=25;
	public Player player;
    public Enemy enemy;
	private AudioSource[] zombieSound;
    

    public void Hit(int damage){

        enemy.GetComponent<EnemyAI>().distanceTrashold = 10000.0F;

        Debug.Log("hit");
        health -= damage;
		zombieSound=GetComponents<AudioSource>();
		zombieSound[1].Play();
		if(health<=0 && enemy.GetComponent<EnemyAI>().aiState != AIState.dead)
        {
			
			player.kills=player.kills+1;
            enemy.GetComponent<EnemyAI>().aiState = AIState.dead;
            enemy.GetComponent<BoxCollider>().enabled = false;
			enemy.GetComponent<AudioSource>().enabled= false;
        }
	}
	
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}

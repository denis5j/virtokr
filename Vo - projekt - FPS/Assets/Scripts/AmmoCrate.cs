﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmmoCrate : MonoBehaviour
{
    
	public GameObject container;
    public float rotationSpeed = 180f;
	public int ammo = 10;
    public bool isCollected = false;
	
    // Update is called once per frame
    void Update()
    {
        container.transform.Rotate(Vector3.up*rotationSpeed*Time.deltaTime);
    }
}

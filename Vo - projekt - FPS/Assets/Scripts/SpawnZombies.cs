﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnZombies : MonoBehaviour
{
    // Start is called before the first frame update

    public GameObject zombie;

    double spawnTime = 0.0;
    bool isSpawned = false;

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (!isSpawned)
        {
            SpawnZombie();
            
            isSpawned = true;
        }

        if (spawnTime > 5)
        {
            isSpawned = false;
            spawnTime = 0.0;
        }

        spawnTime += Time.deltaTime;
    }

    void SpawnZombie()
    {
        Instantiate(zombie, transform.position, transform.rotation);
        Instantiate(zombie, transform.position, transform.rotation);
        Instantiate(zombie, transform.position, transform.rotation);
        Instantiate(zombie, transform.position, transform.rotation);
        Instantiate(zombie, transform.position, transform.rotation);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyAI : MonoBehaviour
{

    NavMeshAgent nm;
    public Transform target;

    public float distanceTrashold = 10f;
 
    public float attackTrashold = 1f;
    
    public enum AIState { idle,chasing,attack,dead};

    public AIState aiState = AIState.idle;

    public Animator animator;

    public bool dead = false;

    public float distance;
    public Player player;

    private double attackTime;
    private bool didAttack = false;
	
	private AudioSource[] hitSound;


    // Start is called before the first frame update
    void Start()
    {
        nm = GetComponent<NavMeshAgent>();
        StartCoroutine(Think());
        target = GameObject.FindGameObjectWithTag("Player").transform;
    }

    // Update is called once per frame
    void Update()
    {
        if (didAttack)
        {
            attackTime += Time.deltaTime;
        }

        if (attackTime > 0.5)
            didAttack = false;
    }

    IEnumerator Think()
    {
        while(true)
        {
            if (dead)
                aiState = AIState.dead;

            switch (aiState)
            {
                case AIState.idle:
                    distance = Vector3.Distance(target.position, transform.position);
                    if(distance < distanceTrashold)
                    {
                        aiState = AIState.chasing;
                        animator.SetBool("Chasing", true);
                    }
                    nm.SetDestination(transform.position);
                    break;
                case AIState.chasing:
                    animator.SetBool("Attacking", false);
                    animator.SetBool("Chasing", true);
                    distance = Vector3.Distance(target.position, transform.position);
                    if (distance > distanceTrashold)
                    {
                        aiState = AIState.idle;
                        animator.SetBool("Chasing", false);
                    }
                    if(distance < attackTrashold)
                    {
                        aiState = AIState.attack;
                        animator.SetBool("Attacking", true);
                    }
                    nm.SetDestination(target.position);
                    break;
                case AIState.attack:
                    animator.SetBool("Attacking", true);
                    animator.SetBool("Chasing", false);
                    nm.SetDestination(transform.position);
                    distance = Vector3.Distance(target.position, transform.position);
                    if(!didAttack)
                    {
                        player.GetComponent<Player>().health -= 5;
						hitSound=GetComponents<AudioSource>();
						hitSound[2].Play();
                        attackTime = 0.0;
                        didAttack = true;
                    }
                    //player.OnTriggerEnter(null);
                    if (distance > attackTrashold)
                    {
                        aiState = AIState.chasing;
                        animator.SetBool("Attacking", false);
                    }
                    break;
                case AIState.dead:
                    nm.SetDestination(transform.position);
                    animator.SetBool("Dead", true);
                    break;
                default:
                    break;
            }
            
            yield return new WaitForSeconds(0f);
        }
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour
{
	
	public GameObject gun;
	
	
	[Header("Visuals")]
	public Camera playerCamera;
	public Text ammoText;
	public Text healthText;
	public Text killsText;
	
	[Header("Gameplay")]
	public int initialAmmo=8;
	public int initialHealth=100;
	public float knockbackForce=10;
	public float hurtDuration=0.5f;
	
	[Header("UI")]
	public int ammo;
	public int health;
	private bool isHurt=false;
	
	public int kills;
	
	
	//public int Kills{set{kills++;}}
	
	

		
	
	
	//ammoText.text= "Ammo: " +ammo;

	//public int ammo;
    // Start is called before the first frame update
    void Start()
    {
		ammo=initialAmmo;
		health=initialHealth;
		kills=0;
        //ammo=initialAmmo;
    }

    // Update is called once per frame
    void Update()
    {
        if (health < 1)
        {
            health = 0;
            Die();
        }
        
        ammoText.text= "Ammo: " +ammo;
		healthText.text="Health: " +health;
		killsText.text="Kills: " +kills;
	
    }
	void Die()
    {
        Scene scene = SceneManager.GetActiveScene();
        SceneManager.LoadScene(scene.name);
    }
	void OnControllerColliderHit (ControllerColliderHit hit){

        //Collect ammo crates
        if (hit.collider.GetComponent<AmmoCrate>() != null && !hit.collider.GetComponent<AmmoCrate>().isCollected)
        {
            hit.collider.GetComponent<AmmoCrate>().isCollected = true;
            Debug.Log("hit crate");
			AmmoCrate ammoCrate = hit.collider.GetComponent<AmmoCrate>();
			Debug.Log(ammoCrate.ammo);
			ammo = ammo + ammoCrate.ammo;
			Destroy (ammoCrate.gameObject);
		}
	}
	
	//Check collisions
	//public void OnTriggerEnter (Collider otherCollider){
	//	//Collect ammo crates
	//	//if(otherCollider.GetComponent<AmmoCrate>() !=null){
	//		//Debug.Log("Collide with ammo");
	//		//AmmoCrate ammoCrate = otherCollider.GetComponent<AmmoCrate>();
			
	//	// Touching Enemies
	//	if(otherCollider.GetComponent<Enemy>() !=null){
	//		if(isHurt==false){
	//			Enemy enemy =otherCollider.GetComponent<Enemy>();
	//			health -= enemy.damage;
	//			isHurt=true;
					
	//			//Knockback effect	
	//			Vector3 hurtDirection=(transform.position-enemy.transform.position).normalized;
	//			Vector3 knockbackDirection=(hurtDirection+Vector3.up).normalized;
	//			GetComponent<ForceReceiver>().AddForce(knockbackDirection,knockbackForce);
				
	//			StartCoroutine(HurtRoutine());
	//		}
			
	//	}  
	
	//}
    public void ReduceAmmo()
    {
        ammo -= 1;
    }
	
	IEnumerator HurtRoutine(){
		yield return new WaitForSeconds(hurtDuration);
		isHurt=false;
	}
	
		
}
